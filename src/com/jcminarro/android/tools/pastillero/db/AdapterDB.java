package com.jcminarro.android.tools.pastillero.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AdapterDB {

	private static final String LOG_TAG = AdapterDB.class.getSimpleName();
	
	public static final String KEY_MEDICAMENT_ID = "id";
	public static final String KEY_TOMA_ID = "id";

	public static final String KEY_MEDICAMENT_NAME = "name";
	public static final String KEY_MEDICAMENT_TYPE = "name";

	public static final String KEY_TOMA_DATE = "date";
	public static final String KEY_TOMA_TAKEN = "taken";
	public static final String KEY_TOMA_AMOUNT = "amount";

	public static final String FOREIGNKEY_TOMA_MEDICAMENT = "medicament";

	private static final int VERSION_DATABASE = 1;

	private static final String NAME_DATABASE = "Pastillero";
	public static final String TABLE_MEDICAMENTS = "medicaments";
	public static final String TABLE_TOMAS = "tomas";
	

	private static final String CREATE_MEDICAMENTS = "create table " +  TABLE_MEDICAMENTS + " (" + KEY_MEDICAMENT_ID + " integer primary key autoincrement, " + KEY_MEDICAMENT_NAME + " text not null, " + KEY_MEDICAMENT_TYPE + " text not null);";
	private static final String CREATE_TOMAS = "create table " +  TABLE_TOMAS + " (" + KEY_TOMA_ID + " integer primary key autoincrement, " + KEY_TOMA_AMOUNT + " integer, " + KEY_TOMA_DATE + " integer, " + FOREIGNKEY_TOMA_MEDICAMENT + " integer, foreign key (" + FOREIGNKEY_TOMA_MEDICAMENT + ") references " + TABLE_MEDICAMENTS + " (" + KEY_MEDICAMENT_ID + "));";
	
	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public AdapterDB(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}
	
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		private static final String LOG_TAG = AdapterDB.LOG_TAG + "." + DatabaseHelper.class.getSimpleName();
		
		DatabaseHelper(Context context) {
			super(context, NAME_DATABASE, null, VERSION_DATABASE);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d(LOG_TAG, "onCreate(SQLiteDatabase db)");
			try {
				db.execSQL(CREATE_MEDICAMENTS);
				db.execSQL(CREATE_TOMAS);
			} catch (SQLException e) {
				Log.e(LOG_TAG, "onCreate(SQLiteDatabase db)", e);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.d(LOG_TAG, "onUpgrade( SQLiteDatabase db, " + oldVersion + ", " + newVersion + ")");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOMAS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICAMENTS);
			onCreate(db);
		}
	}

	public AdapterDB open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		DBHelper.close();
	}

	public long insertMedicament(String name, String type) {
		ContentValues cv = new ContentValues();
		cv.put(KEY_MEDICAMENT_NAME, name);
		cv.put(KEY_MEDICAMENT_TYPE, type);
		return db.insert(TABLE_MEDICAMENTS, null, cv);
	}
	
	public long insertToma(int amount, int date, int medicamentID) {
		ContentValues cv = new ContentValues();
		cv.put(KEY_TOMA_AMOUNT, amount);
		cv.put(KEY_TOMA_DATE, date);
		cv.put(FOREIGNKEY_TOMA_MEDICAMENT, medicamentID);
		return db.insert(TABLE_TOMAS, null, cv);
	}
	
	public Cursor getMedicaments() {
		Cursor c = null;
		if (db != null)
			c = db.query(TABLE_MEDICAMENTS, new String[] { KEY_MEDICAMENT_ID, KEY_MEDICAMENT_NAME,
				KEY_MEDICAMENT_TYPE }, null, null, null, null, null);
		return c;
	}

	public Cursor getTomas(long medicamentID) throws SQLException {
		Cursor mCursor = null;
		if (db != null)
			mCursor = db.query(true, TABLE_TOMAS, new String[] {
				KEY_TOMA_AMOUNT, KEY_TOMA_DATE, KEY_TOMA_TAKEN }, FOREIGNKEY_TOMA_MEDICAMENT + "=" + medicamentID,
				null, null, null, null, null);
		return mCursor;
	}
}
