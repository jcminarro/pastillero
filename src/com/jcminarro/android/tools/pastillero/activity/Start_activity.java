
package com.jcminarro.android.tools.pastillero.activity;

import com.jcminarro.android.tools.pastillero.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;

public class Start_activity extends Activity {

	Button bAddMedicament;
	Button bAddToma;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		bAddMedicament = (Button)findViewById(R.id.bAddMedicament);
		bAddToma = (Button)findViewById(R.id.bAddToma);
	}

}
