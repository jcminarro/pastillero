package com.jcminarro.android.tools.pastillero.activity;

import java.util.regex.Pattern;

import com.jcminarro.android.tools.pastillero.R;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ShareCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class Tabs_activity extends Activity{ 
	LocalActivityManager lam;
	private static final String LOG_TAG = Tabs_activity.class.getSimpleName();

	private TabHost tabHost;
    
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.tabhost);
            Resources res = getResources();
           
            tabHost = (TabHost) findViewById(R.id.tabhost);
            lam = new LocalActivityManager(this, true);

            tabHost.setup(lam);
            lam.dispatchCreate(savedInstanceState); //after the tab's setup is called, you have to call this or it wont work

            
//            tabHost.addTab(tabHost.newTabSpec(res.getString(R.string.tab_tomas)) 
//        			.setIndicator(prepareTabView(res.getString(R.string.tab_tomas), R.drawable)) 
//        			.setContent(new Intent(this,CarteleraApp_activity.class)));
//        	
//        	tabHost.addTab(tabHost.newTabSpec(res.getString(R.string.tabRecientes)) 
//        			.setIndicator(prepareTabView(res.getString(R.string.tabRecientes), R.drawable.btn_recent_history_off)) 
//        			.setContent(new Intent(this,Recent_activity.class)));
//        	
//        	tabHost.addTab(tabHost.newTabSpec(res.getString(R.string.tabFavoritos)) 
//        			.setIndicator(prepareTabView(res.getString(R.string.tabFavoritos), R.drawable.btn_star_off)) 
//        			.setContent(new Intent(this,Favorites_activity.class)));

        	setColorText(0);
        	tabHost.setOnTabChangedListener(new OnTabChangeListener() {
				
				@Override
				public void onTabChanged(String tabId) {
					Log.d(LOG_TAG, "tabId: " + tabId);
					Log.d(LOG_TAG, "Pos: " + tabHost.getCurrentTab());
					setColorText(tabHost.getCurrentTab());
				}
			});
        	
        	
    }

    @Override
    protected void onPause() {
            super.onPause();
            lam.dispatchPause(isFinishing()); //you have to manually dispatch the pause msg
    }

    @Override
    protected void onResume() {
            super.onResume();
            lam.dispatchResume(); //you have to manually dispatch the resume msg
// TODO: Change color tab dynamically            tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundDrawable(Util.getBackgroundSplash()); // http://stackoverflow.com/questions/8481322/create-a-radial-gradient-programmatically
            

    }
	
    private void setColorText(int index){
//    	View v;
//    	int colorTheme = Util.getColorTheme();
//    	int colorBlack = getResources().getColor(R.color.black);
//    	for (int i=0; i<3; i++){
//    		v = tabHost.getTabWidget().getChildTabViewAt(i);
//    		((TextView)v.findViewById(R.id.TabTextView)).setTextColor((index==i) ? colorTheme : colorBlack);
//    	}
//    	
    	
    }
    
//	private View prepareTabView(String text, int resId) {
//		Log.d(LOG_TAG, "prepareTabView(" + text + ", " + resId + ")");
//		View view = LayoutInflater.from(this).inflate(R.layout.tab, null);
//        ImageView iv = (ImageView) view.findViewById(R.id.TabImageView);
//        TextView tv = (TextView) view.findViewById(R.id.TabTextView);
//        iv.setImageResource(resId);
//        tv.setText(text);
//        return view;
//   }

} 